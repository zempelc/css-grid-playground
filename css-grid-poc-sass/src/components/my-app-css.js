/**
@license
Copyright 2018 Google Inc. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
import {html} from '@polymer/lit-element/lit-element.js';

export const style = html`<style>.container{height:100vh;display:-ms-grid;display:grid;-ms-grid-columns:100px 2fr 1fr;grid-template-columns:100px 2fr 1fr;-ms-grid-rows:50px 1fr;grid-template-rows:50px 1fr;background:seagreen}.navbar{background:maroon;width:100%;height:100%;-ms-grid-column:1;-ms-grid-column-span:3;grid-column:1 / 4;-ms-grid-row:1;-ms-grid-row-span:1;grid-rows:1 / 2}.sidebar{background:tomato;-ms-grid-column:1;-ms-grid-column-span:1;grid-column:1 / 2;-ms-grid-row:2;-ms-grid-row-span:1;grid-rows:2 / 3}.content{background:blue;-ms-grid-column:2;-ms-grid-column-span:1;grid-column:2 / 3;-ms-grid-row:2;-ms-grid-row-span:1;grid-rows:2 / 3;display:-ms-grid;display:grid;grid-template-columns:repeat(auto-fit, minmax(100px, 1fr))}.item{background:yellowgreen;min-width:100px;max-height:100px;border:2px solid black}
</style>`;
